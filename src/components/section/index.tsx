import styled from 'styled-components'
import Section from './component'

const SectionSC = styled(Section)`
  .section__disclaimer {
    text-align: center;
    font-size: 11px;
    font-weight: 100;
    font-style: italic;
  }
`

export default SectionSC
