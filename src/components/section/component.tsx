import React, { FC, ElementType } from 'react'
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'
import { ThemeStyle } from '@material-ui/core/styles/createTypography'

interface Props {
  className?: string;
  title: string;
  titleVariant?: ThemeStyle;
  id?: string;
  noMarginCollapse?: boolean;
  component?: ElementType;
  disclaimer?: string;
}

const Section: FC<Props> = (props) => {
  const {
    titleVariant = 'h4',
    noMarginCollapse = true,
    component = 'div',
  } = props

  const borderStyle = noMarginCollapse ? '1px solid transparent' : ''

  const style = {
    borderTop: borderStyle,
    borderBottom: borderStyle,
    borderRadius: '0',
  }

  return (
    <Box
      className={props.className}
      id={props.id}
      style={style}
      component={component}
    >
      <Box my={4}>
        <Box mb={1}>
          <Typography
            variant={titleVariant}
            component="p"
            align="center"
            style={{ fontFamily: 'Julius Sans One' }}
          >
            {props.title}
          </Typography>
          {props.disclaimer && (
            <Typography className="section__disclaimer">
              {props.disclaimer}
            </Typography>
          )}
        </Box>
        {props.children}
      </Box>
    </Box>
  )
}

export default Section
