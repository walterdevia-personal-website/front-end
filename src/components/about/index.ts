import styled from 'styled-components'
import withTheme from '@material-ui/styles/withTheme'
import { Theme } from '@material-ui/core'

// Own
import About from './component'

const AboutStyled = styled(About)`
  padding-top: 50px;

  .about__text-section {
    padding-left: 16px;
    padding-right: 16px;
    text-align: center;
    margin-bottom: ${(props) => (props.theme as Theme).spacing(1)}px;
  }

  .about__title {
    margin-bottom: ${(props) => (props.theme as Theme).spacing(2)}px;
  }

  .about__divider {
    margin: ${(props) => (props.theme as Theme).spacing(3)}px auto;
    max-width: 600px;
  }

  .about__chips-container {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;

    .about__chip {
      line-height: normal;
      margin: ${(props) => (props.theme as Theme).spacing(1)}px;
    }
  }
`

export default withTheme(AboutStyled)
