import React, { FC, useEffect } from 'react'
import { Chip, Box, Typography } from '@material-ui/core'
import Divider from '@material-ui/core/Divider'
import { ThemeStyle } from '@material-ui/core/styles/createTypography'
import { BLOCKS } from '@contentful/rich-text-types'
import {
  documentToReactComponents,
  Options as RendererOptions,
  NodeRenderer,
} from '@contentful/rich-text-react-renderer'

// Own
import Section from 'components/section'
import { withContent, Content } from 'common/content'

const words = [
  'React',
  'NodeJs',
  'TypeScript',
  'Self-taught',
  'Pizza',
  'Bulldogs',
  'My wife',
  'My family',
]

interface Props {
  className?: string;
  content: Content;
}

function useSmoothScrollToTop() {
  useEffect(() => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    })
  }, [])
}

function getHeadingRenderOptions(variant: ThemeStyle): NodeRenderer {
  return (_, children) => {
    return (
      <Typography
        variant={variant}
        component="p"
        align="center"
        className="about__title"
        style={{ fontFamily: 'Julius Sans One' }}
      >
        {children}
      </Typography>
    )
  }
}

const options: RendererOptions = {
  renderNode: {
    [BLOCKS.PARAGRAPH]: (_, children) => (
      <Typography className="about__text-section justify">
        {children}
      </Typography>
    ),
    [BLOCKS.HEADING_1]: getHeadingRenderOptions('h1'),
    [BLOCKS.HEADING_2]: getHeadingRenderOptions('h2'),
    [BLOCKS.HEADING_3]: getHeadingRenderOptions('h3'),
    [BLOCKS.HEADING_4]: getHeadingRenderOptions('h4'),
    [BLOCKS.HR]: () => (
      <div className="about__divider">
        <Divider />
      </div>
    ),
  },
}

const About: FC<Props> = (props) => {
  useSmoothScrollToTop()

  return (
    <Box className={props.className}>
      {documentToReactComponents(props.content.about.details, options)}
      <div className="about__divider">
        <Divider />
      </div>
      <Section title="My self in few words">
        <div className="about__chips-container">
          {words.map((word, index) => (
            <Chip
              key={index}
              label={word}
              className="about__chip"
              variant="outlined"
            />
          ))}
        </div>
      </Section>
    </Box>
  )
}

export default withContent(About)
