import React, { FC } from 'react'
import Box from '@material-ui/core/Box'

// Own
import Section from 'components/section'
import ContactForm from './form'
import ContactLinks from './links'

interface Props {
  className?: string
}

const Contact: FC<Props> = props => {
  return (
    <Section title="Contact me" className={props.className} id="contact-me">
      <Box className="contact__content-container">
        <Box className="contact__content-box">
          <ContactForm />
        </Box>
        <Box className="contact__content-box">
          <ContactLinks />
        </Box>
      </Box>
    </Section>
  )
}

export default Contact
