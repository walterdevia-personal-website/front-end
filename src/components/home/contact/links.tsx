import React, { FC, useContext } from 'react'
import styled from 'styled-components'
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'
import Icon from '@material-ui/core/Icon'

// Own
import { ContentContext, Content } from 'common/content'

const Link = styled('a')`
  color: lightblue;
`

const ContactLinksComponent: FC = () => {
  const { content } = useContext(ContentContext)
  const { contactLinks } = content as Content

  return (
    <Box textAlign="center">
      <Typography>
        <Icon className="vertical-align-middle">email</Icon>{' '}
        <Link
          href={`mailto:${contactLinks.email}?Subject=Contact%20message%20from%20personal%20website`}
          rel="noopener noreferrer"
          target="_blank"
        >
          {contactLinks.email}
        </Link>
      </Typography>
      <Typography>
        <i className="icon-skype"></i> {contactLinks.skype}
      </Typography>
      <Typography>
        <i className="icon-github" style={{ fontSize: '1.3rem' }}></i>{' '}
        <Link
          href={contactLinks.github}
          rel="noopener noreferrer"
          target="_blank"
        >
          {contactLinks.github}
        </Link>
      </Typography>
      <Typography>
        <i className="icon-gitlab"></i>{' '}
        <Link
          href={contactLinks.gitlab}
          rel="noopener noreferrer"
          target="_blank"
        >
          {contactLinks.gitlab}
        </Link>
      </Typography>
      <Typography>
        <Icon className="vertical-align-middle">person</Icon>{' '}
        <Link
          href={contactLinks.curriculum}
          rel="noopener noreferrer"
          target="_blank"
        >
          {contactLinks.curriculum}
        </Link>
      </Typography>
    </Box>
  )
}

export default ContactLinksComponent
