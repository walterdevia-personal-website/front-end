import styled from 'styled-components'
import withTheme from '@material-ui/styles/withTheme'
import { Theme } from '@material-ui/core'

// Own
import Contact from './component'
import hexOrRgbToRgba from 'common/hex-to-rgba'

interface Props {
  theme: Theme
}

const ContactSC = styled(Contact)`
background-color: ${(props: Props) => hexOrRgbToRgba(props.theme.palette.primary.dark, 0.7)};

.contact__content-container {
  padding: 0 ${(props: Props) => props.theme.spacing(4)}px;
  display: flex;
  flex-direction: row;
  flex-wrap: nowrap;
  justify-content: center;
  align-items: center;

  .contact__content-box {
    width: 50%;
  }

  ${(props: Props) => props.theme.breakpoints.down('sm')} {
    flex-wrap: wrap;

    .contact__content-box {
      width: 100%;
    }
  }
}
`

export default withTheme(ContactSC)
