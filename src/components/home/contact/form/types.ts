import { ChangeEvent, SyntheticEvent } from 'react'

export type SubmitResult = null | boolean

export interface Input {
  value: string
  valid: boolean
  errorMessage?: string
}

export interface Inputs {
  name: Input
  subject: Input
  message: Input
}

export interface UseContactForm {
  handleSubmit: (event: SyntheticEvent<any>) => void;
  handleInputChange: (event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => void;
  inputs: Inputs;
}
