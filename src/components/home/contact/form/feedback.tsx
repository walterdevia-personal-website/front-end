import React, { FC } from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'
import Box from '@material-ui/core/Box'
import Icon from '@material-ui/core/Icon'
import withTheme from '@material-ui/core/styles/withTheme'
import { Theme } from '@material-ui/core'
import styled from 'styled-components'

// Own
import { SubmitResult } from './types'
import useSuggestedEmailHelpText from './hooks/use-suggested-email-help-text'

interface Props {
  loading: boolean
  result: SubmitResult
  theme: Theme
}

const StyledIcon = styled(Icon)`
  vertical-align: middle;
`

const Feedback: FC<Props> = props => {
  const shouldUseSuggestedEmailHelpText = useSuggestedEmailHelpText(
    props.result
  )

  return (
    <Box textAlign="center" my={1}>
      {props.loading && (
        <>
          <span>Sending message</span>
          &nbsp;&nbsp;
          <CircularProgress
            size={30}
            style={{
              color: props.theme.palette.secondary.main,
              verticalAlign: 'middle'
            }}
          />
        </>
      )}
      {props.result === false && (
        <>
          <StyledIcon>error</StyledIcon>
          &nbsp;
          <span>
            Sorry but it seems that my server is gone. Please try again.
            <br />
            {shouldUseSuggestedEmailHelpText && (
              <span>
                If the error persist you can send me an email to{' '}
                {process.env.REACT_APP_MY_EMAIL}
              </span>
            )}
          </span>
        </>
      )}
      {props.result === true && (
        <>
          <StyledIcon>check</StyledIcon>
          &nbsp;
          <span>Thank you for your message, I appreciate it.</span>
        </>
      )}
    </Box>
  )
}

export default withTheme(Feedback)
