import { useState } from 'react'

// Own
import { SubmitResult, Inputs } from '../types'
import { areInputsValid } from './use-contact-form'

export default function useSubmitting() {
  const [loading, setLoading] = useState(false)
  const [submitResult, setSubmitResult] = useState<SubmitResult>(null)

  const submitHandler = (inputs: Inputs) => {
    if (!areInputsValid(inputs)) {
      return
    }

    setSubmitResult(null)
    setLoading(true)

    fetch(process.env.REACT_APP_EMAIL_SERVER as string, {
      method: 'POST',
      body: JSON.stringify({
        name: inputs.name.value,
        subject: inputs.subject.value,
        message: inputs.message.value
      }),
      headers: {
        'Content-Type': 'application/json'
      },
      mode: 'cors'
    })
      .then(rawResult => {
        return rawResult.json()
      })
      .then(result => {
        setLoading(false)
        setSubmitResult(result)
      })
      .catch(() => {
        setLoading(false)
        setSubmitResult(false)
      })
  }

  return {
    loading,
    submitResult,
    submitHandler
  }
}