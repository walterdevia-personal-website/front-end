import { useState, SyntheticEvent, ChangeEvent } from 'react'

// Own
import { UseContactForm, Inputs } from '../types'

const emptyValue = {
  value: '',
  valid: true
}

const initialValue: Inputs = {
  name: emptyValue,
  subject: emptyValue,
  message: emptyValue
}

export default function (callback: (values: Inputs) => any): UseContactForm {
  const [inputs, setInputs] = useState<Inputs>(initialValue as any)

  function handleSubmit(event: SyntheticEvent<any>) {
    if (event) {
      event.preventDefault()
    }

    callback(inputs)
  }

  function handleInputChange(event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) {
    event.persist()

    const isValid = isInputValid(event.target)

    setInputs(inputs => ({
      ...inputs,
      [event.target.name]: {
        value: event.target.value,
        valid: isValid,
        errorMessage: !isValid ? 'Input required' : null
      }
    }))
  }

  return {
    handleSubmit,
    handleInputChange,
    inputs
  }
}

function isInputValid(input: HTMLInputElement | HTMLTextAreaElement | string): boolean {
  const value = typeof input === 'string' ? input : input.value
  return !!(value && value.length)
}

export function areInputsValid(inputs: Inputs): boolean {
  return isInputValid(inputs.name.value) &&
    isInputValid(inputs.subject.value) &&
    isInputValid(inputs.message.value)
}
