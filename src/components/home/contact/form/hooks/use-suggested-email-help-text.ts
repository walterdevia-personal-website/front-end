import { useRef, useEffect, useState } from 'react'

// Own
import { SubmitResult } from '../types'

function usePrevValue<T = any>(value: T): T {
  const ref = useRef(value)

  useEffect(() => {
    ref.current = value
  }, [value])

  return ref.current
}

export default function useSuggestedEmailHelpText(submitResult: SubmitResult): boolean {
  const [errorsCount, setErrorsCount] = useState(0)
  const prevSubmitResult = usePrevValue(submitResult)

  useEffect(() => {
    if (prevSubmitResult !== submitResult && submitResult === false) {
      // A new error has happened
      setErrorsCount(() => errorsCount + 1)
    }
  }, [errorsCount, prevSubmitResult, submitResult])

  return submitResult === false && errorsCount > 1
}
