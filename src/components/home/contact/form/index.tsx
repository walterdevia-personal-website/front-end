import React, { FC } from 'react'
import TextField from '@material-ui/core/TextField'
import Box from '@material-ui/core/Box'

// Own
import SubmitButton from './submit-button'
import Feedback from './feedback'
import useContactForm from './hooks/use-contact-form'
import useSubmitting from './hooks/use-submitting'

interface Props {
  className?: string
}

const ContactForm: FC<Props> = props => {
  const { loading, submitResult, submitHandler } = useSubmitting()
  const { handleSubmit, handleInputChange, inputs } = useContactForm(
    submitHandler
  )

  return (
    <form onSubmit={handleSubmit} noValidate className={props.className}>
      <Box mb={1}>
        <TextField
          type="text"
          variant="filled"
          id="name"
          label="Tell me your name, please"
          name="name"
          autoComplete="name"
          fullWidth
          value={inputs.name.value}
          onChange={handleInputChange}
          required
          error={!inputs.name.valid}
          helperText={inputs.name.errorMessage}
          disabled={loading}
        />
      </Box>
      <Box mb={1}>
        <TextField
          type="text"
          variant="filled"
          id="subject"
          label="Tell me your subject (please again)"
          name="subject"
          autoComplete="subject"
          fullWidth
          value={inputs.subject.value}
          onChange={handleInputChange}
          required
          error={!inputs.subject.valid}
          helperText={inputs.subject.errorMessage}
          disabled={loading}
        />
      </Box>
      <Box mb={1}>
        <TextField
          variant="filled"
          id="message"
          label="Your message"
          placeholder="From a simple hello to a deep letter. Feel free to tell me something!"
          name="message"
          autoComplete="message"
          fullWidth
          multiline
          value={inputs.message.value}
          onChange={handleInputChange}
          required
          error={!inputs.message.valid}
          helperText={inputs.message.errorMessage}
          disabled={loading}
        />
      </Box>
      <SubmitButton loading={loading} className="contact-form__button" />
      <Feedback loading={loading} result={submitResult} />
    </form>
  )
}

export default ContactForm
