import React, { FC } from 'react'
import Button from '@material-ui/core/Button'

interface Props {
  className?: string
  loading: boolean
}

const SubmitButton: FC<Props> = props => (
  <Button
    type="submit"
    fullWidth
    variant="contained"
    color="secondary"
    className="contact-form__button"
    disabled={props.loading}
  >
    <span>Send</span>
  </Button>
)

export default SubmitButton
