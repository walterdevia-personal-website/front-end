import styled from 'styled-components'
import withTheme from '@material-ui/styles/withTheme'
import { Theme } from '@material-ui/core'

// Own
import Skills from 'components/home/skills/component'

interface Props {
  theme: Theme;
}

const SkillsSC = styled(Skills)`
.skills-list__title {
  font-family: 'Julius Sans One';
}

.skills-list__separator {
  max-width: 350px;
  margin-left: auto;
  margin-right: auto;
}

.skills-list__cards-container {
  .skills-list__card {
    margin-left: auto;
    margin-right: auto;
  }
}

.skills-list__slider-container {
  &:last-child {
    margin-bottom: 0;
  }

  &:not(:last-child) {
  margin-bottom: ${(props: Props) => props.theme.spacing(4)}px;
  }
}
`

export default withTheme(SkillsSC)
