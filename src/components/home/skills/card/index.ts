import withTheme from '@material-ui/core/styles/withTheme'
import styled from 'styled-components'

// Own
import SkillCard from './component'
import hexOrRgbToRgba from 'common/hex-to-rgba'


const SkillCardStyled = styled(SkillCard)`
position: relative;
max-width: 240px;
width: 100%;
background-color: ${props => hexOrRgbToRgba(props.theme.palette.primary.main)};

  .skill-card__image {
    height: 100px;
    background-size: contain;
  }
`

export default withTheme(SkillCardStyled)
