import React, { FC } from 'react'
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import CardMedia from '@material-ui/core/CardMedia'
import CardContent from '@material-ui/core/CardContent'
import Typography from '@material-ui/core/Typography'

// Types
import { Skill } from 'common/content'

interface Props {
  className?: string
  data: Skill
}

const SkillCard: FC<Props> = props => (
  <Card className={props.className}>
    <CardHeader
      title={props.data.name}
      titleTypographyProps={{
        style: { fontFamily: 'Julius Sans One' }
      }}
    />
    <CardMedia image={props.data.image} className="skill-card__image" />
    <CardContent>
      <Typography variant="body2" component="p">
        {props.data.description}
      </Typography>
    </CardContent>
  </Card>
)

export default SkillCard
