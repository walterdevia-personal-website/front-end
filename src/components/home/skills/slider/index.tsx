import styled from 'styled-components'
import SkillsSlider from './component'

export default styled(SkillsSlider)`
.skills-slider__card {
  margin-left: auto;
  margin-right: auto;
}

.tech-list__dots {
  li {
    button {
      ::before {
        color: white;
      }
    }
  }
}
`
