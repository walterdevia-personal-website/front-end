import React, { FC } from 'react'
import Slider from 'react-slick'
import withTheme from '@material-ui/core/styles/withTheme'
import { Theme } from '@material-ui/core'

// Own
// Hooks
import useSliderSettings from './use-slider-settings'
// Types
import { Skill } from 'common/content'
// Components
import SliderContainer from './container'
import SkillCard from 'components/home/skills/card'

interface Props {
  skills: Skill[]
  className?: string
  theme: Theme
}

const SkillsSlider: FC<Props> = props => {
  const { className, skills } = props

  const sliderSettings = useSliderSettings(
    skills,
    props.theme,
    'slick-dots tech-list__dots'
  )

  if (skills.length === 1) {
    return (
      <div className={className}>
        <SkillCard className="skills-slider__card" data={skills[0]} />
      </div>
    )
  }

  return (
    <SliderContainer className={className} technologiesCount={skills.length}>
      <Slider {...sliderSettings} className="skills-slider__cards-container">
        {skills.map((tech, index) => (
          <SkillCard key={index} data={tech} className="skills-slider__card" />
        ))}
      </Slider>
    </SliderContainer>
  )
}

export default withTheme(SkillsSlider)
