import { useMemo } from 'react'
import { Settings, ResponsiveObject } from 'react-slick'
import { Theme } from '@material-ui/core'

// Own
import { Skill } from 'common/content'

const defaultOptions: Settings = {
  dots: true,
  infinite: true,
  autoplay: true,
  centerMode: false,
  centerPadding: '20px',
  speed: 500,
  slidesToScroll: 1,
  arrows: false,
  lazyLoad: 'ondemand',
  pauseOnHover: true
}

function getBestPossibleSlidesToShow(desiredSlidesToShow: number, items: any[]): number {
  return items.length < desiredSlidesToShow ? items.length : desiredSlidesToShow
}

export default function useSliderSettings(skills: Skill[], theme: Theme, dotsClass = 'slick-dots'): Settings {
  return useMemo<Settings>(() => {
    const responsive: ResponsiveObject[] = [
      {
        breakpoint: theme.breakpoints.values.lg,
        settings: {
          slidesToShow: getBestPossibleSlidesToShow(3, skills)
        }
      },
      {
        breakpoint: theme.breakpoints.values.md - 1,
        settings: {
          slidesToShow: getBestPossibleSlidesToShow(2, skills)
        }
      },
      {
        breakpoint: theme.breakpoints.values.sm,
        settings: {
          slidesToShow: 1
        }
      }
    ]

    return {
      ...defaultOptions,
      dotsClass,
      slidesToShow: skills.length < 4 ? skills.length : 4,
      responsive
    }
  }, [skills, theme, dotsClass])
}
