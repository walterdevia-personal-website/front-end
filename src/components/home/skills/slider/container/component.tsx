import React, { FC } from 'react'
import withTheme from '@material-ui/core/styles/withTheme'
import { Theme, useMediaQuery } from '@material-ui/core'

interface Props {
  technologiesCount: number
  className?: string
  theme: Theme
}

function useMaxWidthStyleForContainer(
  technologiesCount: number,
  theme: Theme
): { maxWidth: string } {
  const isUpSm = useMediaQuery(theme.breakpoints.up('sm'))
  const isUpMd = useMediaQuery(theme.breakpoints.up('md'))
  const isUpLg = useMediaQuery(theme.breakpoints.up('lg'))

  if (isUpLg && technologiesCount >= 4) {
    return { maxWidth: `1070px` }
  }

  if (isUpMd && technologiesCount === 2) {
    return { maxWidth: `540px` }
  }

  if (isUpSm) {
    return { maxWidth: `810px` }
  }

  return { maxWidth: `100%` }
}

const SliderContainer: FC<Props> = props => {
  const maxWidthStyleForContainer = useMaxWidthStyleForContainer(
    props.technologiesCount,
    props.theme
  )
  return (
    <div className={props.className} style={maxWidthStyleForContainer}>
      {props.children}
    </div>
  )
}

export default withTheme(SliderContainer)
