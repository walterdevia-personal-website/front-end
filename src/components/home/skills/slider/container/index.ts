import styled from 'styled-components'
import SliderContainer from './component'

export default styled(SliderContainer)`
  margin-left: auto;
  margin-right: auto;
`