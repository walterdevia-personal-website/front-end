import React, { FC, Fragment, useCallback, ReactElement } from 'react'
import Typography from '@material-ui/core/Typography'
import Divider from '@material-ui/core/Divider'
import Box from '@material-ui/core/Box'

// Own
// Components
import Section from 'components/section'
import SkillsSlider from 'components/home/skills/slider'
// Utils
import { withContent } from 'common/content'
// Types
import { SkillGroups } from './types'
import { Content, Skill, SkillCategory } from 'common/content'

interface Props {
  className?: string
  content: Content
}

function useSkillGroups(skills: Skill[]): SkillGroups {
  return useCallback(() => {
    const skillsGroups: SkillGroups = {} as any

    skills.forEach(x => {
      if (!Array.isArray(skillsGroups[x.category])) {
        skillsGroups[x.category] = []
      }

      skillsGroups[x.category].push(x)
    })

    return skillsGroups
  }, [skills])()
}

const Skills: FC<Props> = props => {
  const { skills } = props.content
  const skillGroups = useSkillGroups(skills)

  function renderSkillGroups(): ReactElement[] {
    const elements = []

    for (const skillCategory in skillGroups) {
      if (skillGroups.hasOwnProperty(skillCategory)) {
        elements.push(
          <Fragment key={skillCategory}>
            <Typography
              variant="h5"
              component="p"
              className="skills-list__title"
              align="center"
            >
              {skillCategory}
            </Typography>
            <Box className="skills-list__separator" mb={2}>
              <Divider light></Divider>
            </Box>
            <div className="skills-list__slider-container">
              <SkillsSlider
                skills={skillGroups[skillCategory as SkillCategory]}
              />
            </div>
          </Fragment>
        )
      }
    }

    return elements
  }

  return (
    <Section className={props.className} title="What I have used" id="skills">
      {renderSkillGroups()}
    </Section>
  )
}

export default withContent(Skills)
