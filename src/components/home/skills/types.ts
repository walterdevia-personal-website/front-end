import { SkillCategory, Skill } from 'common/content'

// export interface Technology {
//   name: string;
//   description: string;
//   image: string;
// }

// export interface TechnologyGroup {
//   name: string;
//   technologies: Technology[];
// }

// export interface SkillGroup {
//   name: SkillCategory;
//   skills: Skill[];
// }

export type SkillGroups = { [key in SkillCategory]: Skill[]; }

// export interface SkillGroup {
//   [key: SkillCategory]: Skill[];
// }