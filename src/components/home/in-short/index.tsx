import React, { FC } from 'react'
import Typography from '@material-ui/core/Typography'

// Own
import Section from 'components/section'
import { withContent, Content } from 'common/content'

interface Props {
  content: Content
}

const InShort: FC<Props> = props => {
  const { content } = props
  return (
    <Section title="In short" id="in-short">
      <Typography component="p" align="center">
        {content.about.inShort}
      </Typography>
    </Section>
  )
}

export default withContent(InShort)
