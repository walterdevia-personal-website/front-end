import React, { FC } from 'react'
import withTheme from '@material-ui/core/styles/withTheme'
import { Theme } from '@material-ui/core'

// Own
// Types
import { Content, withContent } from 'common/content'
// Components
import IntroText from 'components/home/photos-showcase/intro-text'

interface Props {
  className?: string;
  theme: Theme;
  content: Content;
}

const PhotosShowcase: FC<Props> = (props) => {
  return (
    <div className={props.className} id="home">
      <IntroText></IntroText>
    </div>
  )
}

export default withTheme(withContent(PhotosShowcase))
