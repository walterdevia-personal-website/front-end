import { ThemeStyle } from '@material-ui/core/styles/createTypography'

export interface CurrentBreakpoint {
  isSmall: boolean
  isMid: boolean
  isBig: boolean
}

export interface TitlesVariants {
  first: ThemeStyle
  emphasis: ThemeStyle
  main: ThemeStyle
}