import styled from 'styled-components'
import withTheme from '@material-ui/core/styles/withTheme'

// Own
import IntroText from 'components/home/photos-showcase/intro-text/component'

const IntroTextStyled = styled(IntroText)`
  text-align: center;

  .intro-text__name-container {
    opacity: 0;
    transition: opacity 1s ease 0.4s;
    padding-bottom: 75px;

    &.show {
      opacity: 1;
    }

    .intro-text__name {
      font-family: "Julius Sans One";
    }
  }

  .intro-text__text-container {
    padding-bottom: 10px;

    .intro-text__text-1 {
      opacity: 0;
      transition: opacity 0.9s ease 1.4s;

      &.show {
        opacity: 1;
      }
    }

    .intro-text__text-2 {
      opacity: 0;
      transition: opacity 1s ease-out 2.3s;

      &.show {
        opacity: 1;
      }
    }

    .intro-text__main-title {
      display: inline-block;
      opacity: 0;
      transition: opacity 1s ease-in 3.3s;
      font-family: "Julius Sans One";

      &.show {
        opacity: 1;
      }
    }
  }
`

export default withTheme(IntroTextStyled)
