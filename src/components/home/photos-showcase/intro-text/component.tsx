import React, { FC, useEffect, useState } from 'react'
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography'
import withTheme from '@material-ui/core/styles/withTheme'
import { Theme } from '@material-ui/core'
import useMediaQuery from '@material-ui/core/useMediaQuery'

// Own
import { TitlesVariants } from './types'

interface Props {
  className?: string;
  theme: Theme;
}

function useTitlesVariants(theme: Theme): TitlesVariants {
  const isXSmall = useMediaQuery(theme.breakpoints.down('xs'))
  const isSmall = useMediaQuery(theme.breakpoints.between('xs', 'sm'))
  const isMid = useMediaQuery(theme.breakpoints.between('sm', 'md'))

  const titlesVariants: TitlesVariants = {} as any
  titlesVariants.emphasis = 'h5'

  if (isXSmall) {
    titlesVariants.first = 'h5'
    titlesVariants.main = 'h4'
  } else if (isSmall) {
    titlesVariants.first = 'h4'
    titlesVariants.main = 'h3'
  } else if (isMid) {
    titlesVariants.first = 'h3'
    titlesVariants.main = 'h2'
  } else {
    titlesVariants.first = 'h2'
    titlesVariants.main = 'h1'
  }

  return titlesVariants
}

function useShowClassName(): string {
  const [showClassName, setShowClassName] = useState('')

  useEffect(() => {
    setShowClassName('show')
  }, [])

  return showClassName
}

const IntroText: FC<Props> = (props) => {
  const titlesVariants = useTitlesVariants(props.theme)
  const showClassName = useShowClassName()

  return (
    <div className={props.className}>
      <Box className={`intro-text__name-container ${showClassName}`}>
        <Typography variant="h4" display="initial" component="span">
          Hi, I'm
        </Typography>
        &nbsp;
        <Typography
          variant="h2"
          display="initial"
          component="span"
          className="intro-text__name"
        >
          Walter
        </Typography>
        &nbsp;
        <Typography variant="h4" display="initial" component="span">
          ...
        </Typography>
      </Box>
      <Box className="intro-text__text-container" mr={{ xs: 1, sm: 2 }}>
        <Typography align="center" variant={titlesVariants.first} component="p">
          <span className={`intro-text__text-1 ${showClassName}`}>
            A passionate
          </span>
          <br />
          <Box letterSpacing={3} component="span">
            <Typography
              className={`intro-text__text-2 ${showClassName}`}
              variant={titlesVariants.emphasis}
              component="span"
            >
              (and React/NodeJS lover)
            </Typography>
          </Box>
          <br />
          <Typography
            className={`intro-text__main-title ${showClassName}`}
            variant="h2"
            component="span"
          >
            Software Developer
          </Typography>
        </Typography>
      </Box>
    </div>
  )
}

export default withTheme(IntroText)
