import styled from 'styled-components'
import withTheme from '@material-ui/core/styles/withTheme'
import { Theme } from '@material-ui/core/styles'

// Own
import PhotosShowcase from 'components/home/photos-showcase/component'

const PhotosShowcaseStyled = styled(PhotosShowcase)`
  position: relative;
  background-color: rgba(0, 0, 0, 0.4);
  padding-top: 16px;

  ${(props) => (props.theme as Theme).breakpoints.up('sm')} {
    padding-top: 65px;
  }
`

export default withTheme(PhotosShowcaseStyled)
