import React, { FC, Fragment } from 'react'

// Own
// Components
import PhotosShowcase from 'components/home/photos-showcase'
import InShort from 'components/home/in-short'
import Projects from 'components/home/projects'
import Technologies from 'components/home/skills'
import Contact from 'components/home/contact'

const Home: FC = () => (
  <Fragment>
    <PhotosShowcase />
    <InShort />
    <Projects />
    <Technologies />
    <Contact />
  </Fragment>
)

export default Home
