import React, { FC, Fragment, ReactElement } from 'react'

// Own
import { Box, ButtonBase, Typography } from '@material-ui/core'
import { Project } from 'common/content'

interface Props {
  className?: string;
  project: Project;
}

const A: FC<{ proj: Project }> = (props) => (
  <a href={props.proj.url} target="_blank" rel="noopener noreferrer">
    {props.children}
  </a>
)

const ProjectItem: FC<Props> = (props) => {
  return (
    <Box className={props.className}>
      <Box textAlign="center">
        <Typography className="project-item__name" variant="h5">
          {props.project.name}
        </Typography>
        {props.project.url ? (
          <A proj={props.project}>{renderButtonBase(props)}</A>
        ) : (
          <Fragment>{renderButtonBase(props)}</Fragment>
        )}
      </Box>
      <Typography align="justify">{props.project.description}</Typography>
    </Box>
  )
}

function renderButtonBase(props: Props): ReactElement {
  return (
    <ButtonBase disableRipple className="project-item__button">
      <span
        className="project-item__image"
        style={{ backgroundImage: `url(${props.project.image})` }}
      ></span>
    </ButtonBase>
  )
}

export default ProjectItem
