import styled from 'styled-components'
import withTheme from '@material-ui/core/styles/withTheme'
import { Theme } from '@material-ui/core'

// Own
import ProjectItem from './component'
import hexToRgba from 'common/hex-to-rgba'

const ProjectItemSC = styled(ProjectItem)`
  border: 1px solid ${(props) => (props.theme as Theme).palette.primary.light};
  background-color: ${(props) =>
    hexToRgba((props.theme as Theme).palette.primary.dark, 0.8)};

  .project-item__name {
    text-align: center;
    font-family: "Julius Sans One";
  }

  .project-item__button {
    position: relative;
    padding: 10px;
    margin: 10px;
    border-radius: 5px;
    transition: box-shadow ease-in 0.3s;
    transition: transform ease-in 0.1s;
    width: 200px;
    height: 100px;

    ::after {
      content: "";
      border-radius: 5px;
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      box-shadow: 0px 0px 5px 0px rgba(255, 255, 255, 0.75);
      opacity: 0;
      -webkit-transition: all 0.6s cubic-bezier(0.165, 0.84, 0.44, 1);
      transition: all 0.6s cubic-bezier(0.165, 0.84, 0.44, 1);
    }

    &:hover {
      transform: scale(1.07);

      ::after {
        opacity: 1;
      }
    }

    .project-item__image {
      background-position: center;
      background-repeat: no-repeat;
      background-size: contain;
      position: absolute;
      top: 0%;
      left: 50%;
      width: 95%;
      height: 95%;
      transform: translateX(-50%);
      transition: transform 0.1s ease-in;
    }
  }
`

export default withTheme(ProjectItemSC)
