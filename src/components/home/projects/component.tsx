import React, { FC } from 'react'
import Paper from '@material-ui/core/Paper'

// Own
import Section from 'components/section'
import ProjectItem from './project-item'
import { withContent, Content } from 'common/content'
// Data

interface Props {
  className?: string;
  content: Content;
}

const Catalogue: FC<Props> = (props) => (
  <Section
    className={props.className}
    title="What I have done so far"
    disclaimer="* Public projects not compromising companies privacy"
    id="jobs"
    component={Paper}
  >
    <div className="catalogue__products-container">
      {props.content.projects.map((proj, index) => (
        <ProjectItem
          project={proj}
          key={index}
          className="catalogue__product-container"
        ></ProjectItem>
      ))}
    </div>
  </Section>
)

export default withContent(Catalogue)
