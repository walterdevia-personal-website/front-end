import styled from 'styled-components'
import { Theme } from '@material-ui/core'
import withTheme from '@material-ui/core/styles/withTheme'

// Own
import Catalogue from './component'

const CatalogueSC = styled(Catalogue)`
  .catalogue__products-container {
    display: flex;
    flex-wrap: wrap;
    justify-content: center;

    .catalogue__product-container {
      width: 100%;

      ${(props) => (props.theme as Theme).breakpoints.up('sm')} {
        width: 46%;
      }

      ${(props) => (props.theme as Theme).breakpoints.down('xs')} {
        margin: ${(props) => (props.theme as Theme).spacing(1)}px 0;
      }

      padding: ${(props) => (props.theme as Theme).spacing(1)}px;
      margin: ${(props) => (props.theme as Theme).spacing(1)}px;
    }
  }
`

export default withTheme(CatalogueSC)
