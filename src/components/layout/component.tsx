import React, {
  FC,
  useState,
  PureComponent,
  ReactElement
} from 'react'
import CssBaseline from '@material-ui/core/CssBaseline'
import { withRouter, RouteComponentProps } from 'react-router'
import Particles from 'react-particles-js'

// Own
// Components
import Container from '@material-ui/core/Container'
import Header from 'components/layout/header/index'
import Footer from 'components/layout/footer'
import Menu from 'components/layout/menu'
import Preloader from 'components/layout/preloader'
// Hooks
import useEmitWindowResize from './hooks/use-emit-window-resize'
import useHeaderPlaceholderHeight from './hooks/use-header-placeholder-height'
import useLayoutVisibility from './hooks/use-layout-visibility'
import useScrollToSectionWhenShowContent from './hooks/use-scroll-to-section-when-show-content'
import particlesConfig from 'config/particles'

interface Props extends RouteComponentProps {
  className?: string
}

const Layout: FC<Props> = props => {
  useEmitWindowResize(props.location.pathname)
  const [menuOpen, setMenuOpen] = useState(false)
  const headerPlaceholderHeight = useHeaderPlaceholderHeight(
    props.location.pathname
  )
  const { showPreloader, blurPreloader, showContent } = useLayoutVisibility()
  useScrollToSectionWhenShowContent(showContent, props.location.hash)

  return (
    <div className={props.className}>
      <CssBaseline />
      {showContent && (
        <>
          <Header
            menuOpen={menuOpen}
            onOpenMenu={() => {
              setMenuOpen(true)
            }}
          />
          <div className="layout__container" id="layout-container">
            <LayoutParticles className="layout__particles" />
            <Menu open={menuOpen} openChange={setMenuOpen} />
            <Container
              className="layout__content-container"
              id="layout-content-container"
            >
              <div className="layout__container-opacity">
                <div
                  className="layout__header-placeholder"
                  style={{ height: headerPlaceholderHeight }}
                ></div>
                {props.children}
              </div>
            </Container>
          </div>
          <Footer />
        </>
      )}
      {showPreloader && (
        <Preloader className={`layout__preloader ${blurPreloader && 'blur'}`} />
      )}
    </div>
  )
}

class LayoutParticles extends PureComponent<{ className: string }> {
  public render(): ReactElement {
    return (
      <Particles className={this.props.className} params={particlesConfig} />
    )
  }
}

export default withRouter(Layout)
