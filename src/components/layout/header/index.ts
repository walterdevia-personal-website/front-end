import styled from 'styled-components'
import withTheme from '@material-ui/core/styles/withTheme'
import { Theme } from '@material-ui/core'

// Own
import hexToRgba from 'common/hex-to-rgba'
import Header from 'components/layout/header/component'

const HeaderSC = styled(Header)`
background-color: ${props => hexToRgba((props.theme as Theme).palette.primary.dark, 0.87)};
width: 100%;
position: fixed;
height: 47px;

${props => (props.theme as Theme).breakpoints.down('xs')} {
  background-color: rgba(0,0,0,0);
  box-shadow: none;
}

.header__mobile-menu-button-container {
  ${props => (props.theme as Theme).breakpoints.up('sm')} {
    display: none;
  }

  &.with-opacity {
    opacity: 0.6;
  }
}

.header__toolbar {
  justify-content: space-between;
  overflow-x: auto;

  ${props => (props.theme as Theme).breakpoints.down('xs')} {
    display: none;
  }

  .header__link-separator {
    background-color: white;
    height: 8.5px;
    width: 1px;
  }

  .header__link {
    padding: 8px;
    flex-shrink: 0;
    text-decoration: none;
    color: inherit;
    font-size: 18px;

    &:hover {
      text-decoration: underline;
    }
  }
}
`
export default withTheme(HeaderSC)
