import { useEffect } from 'react'

export default function isScrolledIntoView(el: HTMLElement): boolean {
  const rect = el.getBoundingClientRect()
  const elemTop = rect.top
  const elemBottom = rect.bottom

  // Only completely visible elements return true:
  const isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight)
  // Partially visible elements return true:
  //isVisible = elemTop < window.innerHeight && elemBottom >= 0;
  return isVisible
}

export function useCurrentLinkClass(pathName: string) {
  useEffect(() => {
    const inShortDOMElement = document.getElementById('in-short')
    if (inShortDOMElement) {
      if (isScrolledIntoView(inShortDOMElement)) {

      }
    }
  }, [pathName])
}