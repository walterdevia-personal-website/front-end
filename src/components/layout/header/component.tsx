import React, { FC } from 'react'
import { withRouter, RouteComponentProps } from 'react-router-dom'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'

// Own
import Container from 'components/layout/container'
import MenuToggleButton from 'components/layout/menu/toggle-button'
import NavigationLinks from 'components/layout/menu/navigation-links'

interface Props extends RouteComponentProps {
  className?: string
  onOpenMenu: () => void
  menuOpen: boolean
}

const Header: FC<Props> = props => (
  <AppBar className={props.className}>
    <Container>
      <Toolbar component="nav" variant="dense" className="header__toolbar">
        <NavigationLinks
          linkClassName="header__link"
          withBottomSeparator
          separatorClassName="header__link-separator"
        />
      </Toolbar>
      <MenuToggleButton
        className={`
        header__mobile-menu-button-container ${props.menuOpen &&
          'with-opacity'}`}
        icon="menu"
        onClick={props.onOpenMenu}
      />
    </Container>
  </AppBar>
)

export default withRouter(Header)
