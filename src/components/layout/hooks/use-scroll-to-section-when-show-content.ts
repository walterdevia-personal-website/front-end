import { useState, useEffect } from 'react'

export default function useScrollToSectionWhenShowContent(showContent: boolean, hash: string) {
  const [scrolled, setScrolled] = useState(false)

  useEffect(() => {
    if (showContent && !scrolled) {
      const el: HTMLElement | null = document.getElementById(
        hash.replace('#', '')
      )
      el && el.scrollIntoView({ behavior: 'smooth', block: 'start' })
      setScrolled(true)
    }
  }, [showContent, hash, scrolled])
}
