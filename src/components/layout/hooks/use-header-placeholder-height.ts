export default function useHeaderPlaceholderHeight(currentPath: string): string {
  return currentPath && currentPath.includes('about') ? '47px' : '0'
}