import { useContext, useState, useEffect } from 'react'

// Own
import { ContentContext } from 'common/content'

export interface LayoutVisibility {
  showContent: boolean
  showPreloader: boolean
  blurPreloader: boolean
}

export default function useLayoutVisibility(): LayoutVisibility {
  const { content } = useContext(ContentContext)
  const [blurPreloader, setBlurPreloader] = useState(false)
  const [showPreloader, setShowPreloader] = useState(true)

  useEffect(() => {
    if (content) {
      setBlurPreloader(true)
      setTimeout(() => {
        setShowPreloader(false)
      }, 500)
    }
  }, [content])

  return {
    showContent: !!content,
    showPreloader,
    blurPreloader
  }
}