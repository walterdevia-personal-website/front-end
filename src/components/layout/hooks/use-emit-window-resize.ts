import { useEffect } from 'react'

export default function useEmitWindowResize(pathName: string) {
  // When the height of the window changes the background with particles must be readjusted.
  // And the height of the window changes when new route is rendered.
  // Since it doesn't works automatically it needs to be performed manually.
  useEffect(() => {
    window.dispatchEvent(new Event('resize')) // Simulate window resizing
  }, [pathName]) // When the pathname (route) changes
}