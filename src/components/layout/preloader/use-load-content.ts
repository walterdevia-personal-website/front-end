import { useState, useCallback } from 'react'

// Own
// Types
import { Content } from 'common/content'

export interface ProgressState {
  loading: boolean
  error: string | null
  contentLoaded: boolean
}

export interface ContentLoaderData extends ProgressState {
  loadContent: () => Promise<void>;
}

export default function useLoadContent(onLoad: (content: Content) => any): ContentLoaderData {
  const [progress, setProgress] = useState<ProgressState>({
    loading: false,
    error: null,
    contentLoaded: false
  })

  const loadContent = useCallback(
    async function loadContent() {
      setProgress({ loading: true, error: null, contentLoaded: false })

      const result = await fetch(
        process.env.REACT_APP_CONTENT_SERVER as string,
        {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json'
          },
          mode: 'cors'
        }
      )
        .then((result: Response) => result.json())
        .catch((error: string) => new Error(error))

      const thereWasAnError = result instanceof Error
      setProgress({
        loading: false,
        error: thereWasAnError ? result.toString() : null,
        contentLoaded: !thereWasAnError
      })

      if (!thereWasAnError) {
        onLoad(result as Content)
      }
    },
    [onLoad]
  )

  return {
    ...progress,
    loadContent
  }
}