import React, { FC, useEffect, useContext } from 'react'
import Paper from '@material-ui/core/Paper'
import LinearProgress from '@material-ui/core/LinearProgress'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import Icon from '@material-ui/core/Icon'

// Own
// Common
import { ContentContext } from 'common/content'
// Hooks
import useLoadContent from './use-load-content'

interface Props {
  className?: string
}

const Preloader: FC<Props> = props => {
  const { updateContent } = useContext(ContentContext)
  const contentLoaderData = useLoadContent(updateContent)
  const { loading, contentLoaded, error, loadContent } = contentLoaderData

  useEffect(() => {
    if (!loading && !contentLoaded && !error) {
      loadContent()
    }
  }, [loading, contentLoaded, error, loadContent])

  return (
    <Paper className={props.className}>
      <div className="preloader__inner-container">
        {!error && (
          <>
            <Typography variant="h5">I'm on it...</Typography>
            <LinearProgress color="secondary" />
          </>
        )}
        {error && (
          <>
            <Typography variant="h5">
              <span>
                Ups, I had a problem loading my content. You can retry or write
                me to my email
              </span>{' '}
              <code className="preloader__email">
                {process.env.REACT_APP_MY_EMAIL}
              </code>
            </Typography>
            <Button variant="contained" onClick={() => loadContent()}>
              Reload <Icon>refresh</Icon>
            </Button>
          </>
        )}
      </div>
    </Paper>
  )
}

export default Preloader
