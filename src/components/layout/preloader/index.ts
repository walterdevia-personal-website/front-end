import styled from 'styled-components'

// Own
import Preloader from './component'

export default styled(Preloader)`
position: fixed;
height: 100vh;
width: 100vw;
vertical-align: middle;
text-align: center;
top: 0;
left: 0;
display: flex;
flex-flow: row;
flex-wrap: wrap;
align-items: center;
justify-content: center;

.preloader__inner-container {
  max-width: 600px;
  width: 100%;

  .preloader__email {
    background-color: rgba(255,255,255,0.4);
    padding: 2px;
    border-radius: 4px;
  }
}

`
