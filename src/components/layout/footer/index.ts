import styled from 'styled-components'
import Footer from './component'

export default styled(Footer)`
.heart-icon {
  vertical-align: middle;
  color: red;
}

.link {
  color: lightblue;
}

.tech-logo {
  display: inline;
  padding-left: 5px;
  padding-right: 5px;
  vertical-align: middle;
  width: 35px;
}
`
