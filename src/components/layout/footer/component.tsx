import React, { FC } from 'react'
import { Box, Paper, Typography } from '@material-ui/core'
import Icon from '@material-ui/core/Icon'

// Own
import reactLogo from 'assets/react-logo'
import styledComponentsLogo from 'assets/styled-components-logo'

interface Props {
  className?: string
}

const Footer: FC<Props> = props => (
  <Paper square className={props.className}>
    <Box py={3}>
      <Typography align="center">
        This website is handmade with{' '}
        <Icon className="heart-icon">favorite</Icon>&nbsp; with &nbsp;
        <a
          className="link"
          href="https://reactjs.org"
          rel="noopener noreferrer"
          target="_blank"
        >
          React
          <img src={reactLogo} alt="small react logo" className="tech-logo" />
        </a>
        ,&nbsp;
        <a
          className="link"
          href="https://www.styled-components.com"
          rel="noopener noreferrer"
          target="_blank"
        >
          styled-components
          <img
            src={styledComponentsLogo}
            alt="small react logo"
            className="tech-logo"
          />
        </a>
        and&nbsp;
        <a
          className="link"
          href="https://material-ui.com/"
          rel="noopener noreferrer"
          target="_blank"
        >
          Material UI
        </a>
        .
      </Typography>
      <Typography align="center">
        <span>Check the code of this website at its</span>{' '}
        <a
          className="link"
          href={process.env.REACT_APP_PROJECT_REPO}
          rel="noopener noreferrer"
          target="_blank"
        >
          GitHub repo
        </a>
        .
      </Typography>
    </Box>
  </Paper>
)

export default Footer
