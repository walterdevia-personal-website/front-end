import React, { FC, Fragment } from 'react'
import { HashLink as Link } from 'react-router-hash-link'
import { Typography } from '@material-ui/core'
import { ThemeStyle } from '@material-ui/core/styles/createTypography'

interface LinkItem {
  text: string
  target: string
}

const links: LinkItem[] = [
  {
    target: '/#home',
    text: 'Home'
  },
  {
    target: '/#in-short',
    text: 'In short'
  },
  {
    target: '/#jobs',
    text: 'Jobs'
  },
  {
    target: '/#skills',
    text: 'Skills'
  },
  {
    target: '/about',
    text: 'About'
  },
  {
    target: '/#contact-me',
    text: 'Contact me'
  }
]

interface Props {
  linkClassName?: string
  linkVariant?: ThemeStyle
  separatorClassName?: string
  withBottomSeparator?: boolean
  onLinkClick?: () => any
}

function isLastNavigationLink(index: number): boolean {
  return index === links.length - 1
}

function smoothScroll(el: HTMLElement) {
  el.scrollIntoView({ behavior: 'smooth', block: 'start' })
}

function clickFallback() {
  //
}

const NavigationLinks: FC<Props> = props => (
  <Fragment>
    {links.map((link, index) => (
      <Fragment key={index}>
        <Link
          to={link.target}
          className={props.linkClassName}
          scroll={smoothScroll}
          onClick={props.onLinkClick || clickFallback}
        >
          <Typography
            variant={props.linkVariant || 'subtitle1'}
            component="span"
          >
            {link.text}
          </Typography>
        </Link>
        {props.withBottomSeparator && !isLastNavigationLink(index) && (
          <div className={props.separatorClassName} />
        )}
      </Fragment>
    ))}
  </Fragment>
)

export default NavigationLinks
