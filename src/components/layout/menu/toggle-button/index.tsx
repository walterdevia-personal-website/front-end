import React, { FC } from 'react'
import { IconButton, Icon } from '@material-ui/core'
import styled from 'styled-components'

interface Props {
  className?: string
  icon: 'menu' | 'close'
  onClick: () => void
}

const Div = styled('div')`
  text-align: right;
`

const ToggleButton: FC<Props> = props => (
  <Div className={props.className}>
    <IconButton color="secondary" onClick={props.onClick}>
      <Icon>{props.icon}</Icon>
    </IconButton>
  </Div>
)

export default ToggleButton
