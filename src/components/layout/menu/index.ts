import styled from 'styled-components'
import { Theme } from '@material-ui/core'
import withTheme from '@material-ui/core/styles/withTheme'

// Own
import Component from './component'
import hexToRgba from 'common/hex-to-rgba'

const MenuSC = styled(Component)`
  background-color: ${props => hexToRgba((props.theme as Theme).palette.primary.light, 0.8)};

  .menu__link {
    display: block;
    text-decoration: none;
    color: ${props => (props.theme as Theme).palette.text.primary};
    margin-bottom: 8px;
    text-align: center;

    &:hover {
      color: ${props => (props.theme as Theme).palette.text.secondary};
    }
  }
`

export default withTheme(MenuSC)
