import React, { FC } from 'react'
import { slide as BurgerMenu } from 'react-burger-menu'

// Own
import MenuToggleButton from 'components/layout/menu/toggle-button'
import NavigationLinks from './navigation-links'

type OpenChange = (open: boolean) => void

interface Props {
  className?: string
  open: boolean
  openChange: OpenChange
}

let _openChange: OpenChange

function handleMenuStateChange(state: { isOpen: boolean }) {
  _openChange(state.isOpen)
}

function closeMenu() {
  _openChange(false)
}

const Menu: FC<Props> = props => {
  _openChange = props.openChange

  return (
    <BurgerMenu
      right
      className={props.className}
      isOpen={props.open}
      onStateChange={handleMenuStateChange}
      customBurgerIcon={false}
      customCrossIcon={false}
    >
      <MenuToggleButton icon="close" onClick={closeMenu} />
      <NavigationLinks
        linkClassName="menu__link"
        linkVariant="h5"
        onLinkClick={closeMenu}
      />
    </BurgerMenu>
  )
}

export default Menu
