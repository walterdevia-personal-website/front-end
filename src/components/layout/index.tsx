import styled from 'styled-components'
import { Theme } from '@material-ui/core'
import withTheme from '@material-ui/core/styles/withTheme'

// Own
import Layout from './component'

const LayoutSC = styled(Layout)`
  position: relative;
  width: 100%;
  min-height: 100vh;

  .layout__container {
    min-height: 100vh;
    position: relative;

    .layout__particles {
      position: absolute;
      left: 0;
      top: 0;
      width: 100%;
      height: 100%;
      z-index: -1;
    }

    .layout__content-container {
      .layout__container-opacity {
        background-color: rgba(0, 0, 0, 0.7);
        min-height: 100vh;
        border-left: 1px solid
          ${props => (props.theme as Theme).palette.secondary.main};
        border-right: 1px solid
          ${props => (props.theme as Theme).palette.secondary.main};
      }
    }
  }

  .layout__preloader {
    transition: opacity 0.6s ease-in-out;
    opacity: 1;

    &.blur {
      transition: opacity 0.6s ease-in-out;
      opacity: 0;
    }
  }
`
export default withTheme(LayoutSC)
