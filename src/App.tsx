import React, { useState, useCallback } from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import { ThemeProvider } from '@material-ui/styles'
//
// Sets the MUI styles tag at the top of <head />
// so styles tags from styled components gets more specificity over MUI styles tags.
import { StylesProvider } from '@material-ui/styles'

// Own
// Config
import theme from 'config/theme'
// Components
import Layout from 'components/layout'
import Home from 'components/home'
import About from 'components/about'
// common
import { ContentContext, Content } from 'common/content'

const App: React.FC = () => {
  const [content, setContent] = useState<Content | null>(null)
  const updateContent = useCallback((content: Content) => {
    setContent(content)
  }, [])

  return (
    <ThemeProvider theme={theme}>
      <StylesProvider injectFirst>
        <BrowserRouter>
          <ContentContext.Provider value={{ content, updateContent }}>
            <Layout>
              <Switch>
                <Route component={About} path="/about" />
                <Route component={Home} />
              </Switch>
            </Layout>
          </ContentContext.Provider>
        </BrowserRouter>
      </StylesProvider>
    </ThemeProvider>
  )
}

export default App
