import { Subtract } from 'utility-types'
import React, { createContext, useContext, FC, ComponentType } from 'react'

export type SkillCategory =
  | 'front-end'
  | 'back-end'
  | 'storage'
  | 'languages'
  | 'communication'

export interface Skill {
  name: string
  description: string
  level: 'low' | 'middle' | 'high'
  image: string
  category: SkillCategory
}

export interface Project {
  name: string
  description: string
  url: string
  image: string
  type: 'hobby' | 'real'
}

export interface About {
  inShort: string
  details: any
}

export interface ContactLinks {
  skype: string
  github: string
  gitlab: string
  curriculum: string
  email: string
}

export interface Content {
  mainPhoto: string
  about: About
  projects: Project[]
  skills: Skill[]
  contactLinks: ContactLinks
}

export interface ContentContextType {
  content: Content | null
  updateContent: (content: Content) => any
}

export const ContentContext = createContext<ContentContextType>({
  content: null,
  updateContent: () => {}
})

interface InjectedContentProp {
  content: Content
}

export function withContent<T extends InjectedContentProp>(
  Component: ComponentType<T>
): ComponentType<Subtract<T, InjectedContentProp>> {
  const ComponentWithContent: FC<Subtract<T, InjectedContentProp>> = props => {
    const { content } = useContext(ContentContext)
    return <Component content={content as Content} {...(props as T)} />
  }

  return ComponentWithContent
}
