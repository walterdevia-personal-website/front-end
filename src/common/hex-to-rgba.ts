export default function hexOrRgbToRgba(hexOrRgb: string, opacity = 0.8): string {
  hexOrRgb = hexOrRgb.replace(/ /g, '') // Removes white spaces

  if (hexOrRgb.includes('rgb')) {
    hexOrRgb = hexOrRgb.replace('rgb', 'rgba')
    const rgbArray = hexOrRgb.split('')
    rgbArray.splice(hexOrRgb.length - 1, 0, ',', opacity.toString())
    return rgbArray.join('')
  }

  // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
  let shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i
  hexOrRgb = hexOrRgb.replace(shorthandRegex, function (m, r, g, b) {
    return r + r + g + g + b + b
  })

  let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hexOrRgb)

  if (!result) {
    return ''
  }

  const rgb = {
    r: parseInt(result[1], 16),
    g: parseInt(result[2], 16),
    b: parseInt(result[3], 16)
  }

  return `rgba(${rgb.r},${rgb.g},${rgb.b},${opacity})`
}
