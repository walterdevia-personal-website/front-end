import React from 'react'
import ReactDOM from 'react-dom'
import 'assets/fontello/css/fontello.css'
import 'assets/global.css'
import App from './App'
import { polyfill } from 'es6-promise'
import 'isomorphic-fetch'
import * as serviceWorker from './serviceWorker'

polyfill()

ReactDOM.render(<App />, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
