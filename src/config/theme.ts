import createMuiTheme from '@material-ui/core/styles/createMuiTheme'
import responsiveFontSizes from '@material-ui/core/styles/responsiveFontSizes'

const darkTheme = createMuiTheme({
  typography: {
    fontFamily: [
      'Khula',
      'sans-serif',
      '"Segoe UI"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
  },
  palette: {
    background: {
      paper: '#585858',
      default: '#303030',
    },
    primary: {
      main: '#424242',
      light: 'rgb(103, 103, 103)',
      dark: 'rgb(46, 46, 46)',
      contrastText: '#fff',
    },
    secondary: {
      main: '#C62828',
      light: 'rgb(209, 83, 83)',
      dark: 'rgb(138, 28, 28)',
      contrastText: '#fff',
    },
    error: {
      light: '#e35183',
      main: '#ad1457',
      dark: '#78002e',
      contrastText: '#fff',
    },
    type: 'dark',
  },
})

export default responsiveFontSizes(darkTheme)
